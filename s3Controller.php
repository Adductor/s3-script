<?php

namespace S3Bucket;

// aws bucket control class
class Sync
{
    private $bucketConn;

    function __construct()
    {
        require_once __DIR__ . '/config/awsConfig.php';
        $this->bucketConn = new BucketSource();
    }
    // Download all new files to REPO ready to update DB
    function update($lastEntry) {
        $this->bucketConn->getObjectLastModified($lastEntry);
    }

    // REPO functions - Probably should have own class
    function getLastEntry() {
        return $this->bucketConn->getMostRecent();
    }
    function clearRepo() {
        $this->bucketConn->clearTempBucketFiles();
    }
}