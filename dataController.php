<?php

namespace S3Bucket;

// database control class
class Data
{
	// Repository for temp storage of files
	private $repo;
	private $conn;

    function __construct()
    {
		require_once __DIR__ . "/config/dataSource.php";
    	$this->conn = new Source();
        $this->repo = __DIR__ . "/repo/fm_weather";
    }
	public function updateDBFromRepo() {

		$files = scandir($this->repo);

		if (!empty($files)) {
			foreach ($files as $file) {
				if ($file !== "." && $file !== "..") {
					$isCSV = false;
					$ext = pathinfo($file);
					$ext = $ext['extension'];
					if ($ext == "csv") {
						$isCSV = true;
					}

					$insert = $this->updateWeatherData($file, $isCSV);
					if ($insert !== true) {
						return array("status" => "error", "message" => "There was an error with file (repo) handling!");
					}
				}
			}
		} else {
		 	return array("status" => "error", "message" => "There was an error with file (repo) handling!");
		}
		return array("status" => "success", "message" => "All Files in Repo Processed");
	}

	// insert into weather table
	private function updateWeatherData($data, $isCSV) {
		if ($isCSV) {
		 	$data = $this->processCSV($data);
		}
		if (is_array($data) && !empty($data)) {
		 	foreach ($data as $row) {
		 		if ($row[0] !== "UnixTimestamp" && !empty($row)) {
				$query = 'INSERT IGNORE INTO `weather-data` (`unix_timestamp`, `date`, `time`, `met(mettemp_c)`, `met(metwindspeedAvg_mph)`, `met(metwindspeed3secgust_mph)`, `met(metwinddir_deg)`, `met(metpressure_mb)`, `rainfall_mm`, `rainfalldaily_mm`, `met(metpyranometer_wm2)`, `met(metwindspeedavg_ms)`, `met(metwindspeed3secgust_ms)`, `met(methumidity_pc)`, `met(metdewpoint_c)`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
				$paramType = 'sssssssssssssss';
				$paramValue = array(
					$row[0],
					$row[1],
					$row[2],
					$row[3],
					$row[4],
					$row[5],
					$row[6],
					$row[7],
					$row[8],
					$row[9],
					$row[10],
					$row[11],
					$row[12],
					$row[13],
					$row[14],
				);

				$result = $this->conn->insert($query, $paramType, $paramValue);
				// do nothing
				if (!empty($result)) {
					$status = "inserted";
				} else {
					$status = "exists";
				}
				}
		    }
			return true;
		} else {
			return false;
		}
	}
	
	// convert csv file to object
	private function processCSV($file) {
		$data = fopen($this->repo . "/" . $file, "r");

		 $object = array();
	    while(! feof($data))
		    {
	         	array_push($object, (fgetcsv($data)));
	        }
	  	 fclose($data);
		 return $object;
	}

	public function normaliseData() {
		$lastDate = $this->getLatestDate();
		$now = Date('Y-m-d');

		if (!empty($lastDate)) {
			//echo json_encode($lastDate[0]["MAX(`date`)"]);		
			$prev = $lastDate[0]["MAX(`date`)"];
			
			$normalise = $this->normalise($prev, $now);
			if (!empty($normalise)) {
				return $normalise;
			}
		}
	}
	private function normalise($d1, $d2) {
		$query = 'CALL normalise_data(?, ?);';
		$paramType = 'ss';
		$paramValue = array(
			$d1,
			$d2
		);

		$result = $this->conn->call($query, $paramType, $paramValue);
		// do nothing
	}

	private function getLatestDate() {
		$query = 'SELECT MAX(`date`) FROM `weather-data`';
		$paramType = '';
		$paramValue = array();

		$result = $this->conn->select($query, $paramType, $paramValue);
		// do nothing
		if (!empty($result)) {
			return $result;
		} else {
			return "Normalisation error 1";
		}
	}
}
