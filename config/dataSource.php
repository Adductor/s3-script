<?php

namespace S3Bucket;

// DB Config and Functions
class Source
{
    // define DB here
    private $host = 'localhost';
    private $username = 'root';
    private $password = 'MYSQL_PASSWORD';
    private $databaseName = 'DB_NAME';
    private $conn;
  
    function __construct()
    {
       $this->conn = $this->getConnection();
    }
    // connect
    public function getConnection()
    {
        $conn = new \mysqli($this->host, $this->username, $this->password, $this->databaseName);
        if (mysqli_connect_errno()) {
           trigger_error("Problem with connecting to database.");
        }
        $conn->set_charset("utf8");
        return $conn;
    }
    // basic SQL functions
	public function select($query, $paramType = "", $paramArray = array())
    {
        $stmt = $this->conn->prepare($query);
        if (! empty($paramType) && ! empty($paramArray)) {
            $this->bindQueryParams($stmt, $paramType, $paramArray);
        }
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $resultset[] = $row;
            }
        }
        if (! empty($resultset)) {
            return $resultset;
        }
	}

	public function call($query, $paramType, $paramArray)
    {
        $stmt = $this->conn->prepare($query);
        $this->bindQueryParams($stmt, $paramType, $paramArray);
        $stmt->execute();
        $effected = $stmt->affected_rows;
        return $effected;
    }
    public function insert($query, $paramType, $paramArray)
    {
        $stmt = $this->conn->prepare($query);
        $this->bindQueryParams($stmt, $paramType, $paramArray);
        $stmt->execute();

        $insertId = $stmt->insert_id;
        return $insertId;
    }
    
    
    public function remove($query, $paramType, $paramArray)
    {
        $stmt = $this->conn->prepare($query);
        $this->bindQueryParams($stmt, $paramType, $paramArray);
        $stmt->execute();
        $effected = $stmt->affected_rows;
        return $effected;
    }
    // supporting prepare statement functions
    public function bindQueryParams($stmt, $paramType, $paramArray = array())
    {
        $paramValueReference[] = & $paramType;
        for ($i = 0; $i < count($paramArray); $i ++) {
            $paramValueReference[] = & $paramArray[$i];
        }
        call_user_func_array(array(
            $stmt,
            'bind_param'
        ), $paramValueReference);
    }

    public function execute($query, $paramType = "", $paramArray = array())
    {
        $stmt = $this->conn->prepare($query);
        
        if (! empty($paramType) && ! empty($paramArray)) {
            $this->bindQueryParams($stmt, $paramType, $paramArray);
        }
        $stmt->execute();
    } 
}