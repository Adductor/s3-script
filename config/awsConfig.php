<?php

namespace S3Bucket;

use Aws\S3\S3Client;  
use Aws\Exception\AwsException;
// AWS S3 Bucket Config and Functions
class BucketSource
{
    // define AWS Details here
    // Could be set through environment variables for profile switching
    private $key = "KEY";
    private $secret = "SECRET_KEY";

    private $bucket = 'BUCKET_NAME';
    private $objectSubDir = 'BUCKET_SUBDIRECTORY'; // optional (handle 'folder' in bucket)
    private $region = 'REGION';
    private $version = 'latest';

    // Repository for temp storage of files
    private $repo;

    private $s3Conn;
  
    function __construct()
    {
        $this->s3Conn = $this->getAWSConnection();
    }   
    // create s3Client Object on class contruct
    public function getAWSConnection()
    {
        $this->repo = "./repo";

        require './vendor/autoload.php';

            $s3Client = new S3Client([
                'region' => $this->region,
                'version' => $this->version,
                'credentials' => [
                      'key' => $this->key,
                      'secret' => $this->secret,
                ]
            ]);  

            return $s3Client;
    } 
    // get files (if available) from bucket and save to repo
    public function getObjectLastModified($filename) {

        // define path & prefix - optional/adjustable dependent on bucket objects
        $lastKey = "fm_weather/" . $filename;
        $prefix = 'fm_weather/Unix_';

        // list all objects from bucket since $lastEntry
        $objects = $this->s3Conn->listObjectsV2([
            'Bucket' => $this->bucket,
            'Prefix' => $prefix,
            'StartAfter' => $lastKey
        ]);

        // object array limit 1000 objects so may not fully update in one call   
        if (!empty($objects)) {
                 foreach ($objects as $files) {
                    if (is_array($files)) {
                       foreach ($files as $file) {
                           if (isset($file["Key"])) {
                               // download relevant file from bucket to REPO
                               $this->s3Conn->getObject([
                                   'Bucket' => $this->bucket,
                                   'Key' => $file["Key"],
                                   'SaveAs' => $this->repo . "/" . $file["Key"]
                                ]);
                            } 
                        }
                    }
                 }
        }
    }
    // clear temp files once inserted into DB to save storage
    public function clearTempBucketFiles() {
        // leave most recent file as marker
        $mostRecent = scandir($this->repo . "/fm_weather", SCANDIR_SORT_DESCENDING);
        $newestFile = $mostRecent[0];

        $files = glob($this->repo . "/fm_weather/*");
        if (!empty($files)) {
            foreach($files as $file){
            if(is_file($file) && strpos(strval($file), $newestFile) == false) {
                unlink($file);
            }
            }
        }
    }
    function getMostRecent() {
        $mostRecent = scandir($this->repo . "/fm_weather", SCANDIR_SORT_DESCENDING);
        $newestFile = $mostRecent[0];
        return $newestFile;

    }
}
