<?php 

use S3Bucket\Sync, S3Bucket\Data;

require_once 'dataController.php';
require_once 's3Controller.php';

$process = new Sync();

//    clear REPO of all files but latest - could be done differently or at end of script rather than start
$process->clearRepo();

// get latest file from REPO - marker for S3 bucket
$lastEntry = $process->getLastEntry();

if (!empty($lastEntry)) {
        // get all files from S3 bucket that are more recent than $lastEntry
        // download all new files to REPO
        $process->update($lastEntry);

        // if files are in REPO
        // Insert into DB
        $existingData = new Data();
        $updateDB = $existingData->updateDBFromRepo();

        // end
        if (!empty($updateDB)) {
            
            $updateDB = $existingData->normaliseData(); 
            // echo "Complete";
        }

} else {
    echo "Error: Database step";
}
